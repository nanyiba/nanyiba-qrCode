# nanyiba-qrCode

#### 介绍
二维码授权登陆+websocket点对点发送消息


#### 软件架构
![输入图片说明](https://images.gitee.com/uploads/images/2021/0801/190935_a40d113c_9517144.png "屏幕截图.png")

#### Api文档
https://docs.apipost.cn/preview/b62d27c57bc0048b/7f5e090f50e80c02

#### 入口：二维码授权登陆页面
http://210.22.22.150:5306/nanyiba-qrCode-show/index.html

清除token：http://210.22.22.150:3930/qrCode/delete?mobile=18333333333

#### 部署
1. 将nanyiba-qrCode-show静态资源放置服务器中
2. 打包安装nanyiba-qrCode.jar,传至服务器运行
