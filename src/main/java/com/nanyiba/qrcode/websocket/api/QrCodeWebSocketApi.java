package com.nanyiba.qrcode.websocket.api;

import com.nanyiba.qrcode.websocket.WebSocketSessionOpr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.WebSocketSession;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author 熊腾强
 * @Date 2021/8/1 2:52 下午
 * @Description websocket api
 */
@CrossOrigin
@RestController
public class QrCodeWebSocketApi {

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    /**
     * 服务器指定用户进行推送
     */
    @GetMapping("/qrCodeWs/sendUser")
    public Map<String, Object> sendUser(String token) {
        WebSocketSession webSocketSession = WebSocketSessionOpr.get(token);
        if (webSocketSession != null) {
            simpMessagingTemplate.convertAndSendToUser(token, "/queue/sendUser", "扫码成功,请在手机端确认授权");
        }
        Map<String, Object> res = new HashMap<>(2);
        res.put("success", true);
        res.put("msg", "成功");
        return res;
    }
}
