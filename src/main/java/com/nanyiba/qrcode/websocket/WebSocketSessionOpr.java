package com.nanyiba.qrcode.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.WebSocketSession;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author 熊腾强
 * @Date 2021/8/1 2:11 下午
 * @Description WebSocketSession对象opr
 */
public class WebSocketSessionOpr {

    public static final Logger LOGGER = LoggerFactory.getLogger(WebSocketSessionOpr.class);

    private static ConcurrentHashMap<String, WebSocketSession> manager = new ConcurrentHashMap<String, WebSocketSession>();

    public static void add(String key, WebSocketSession webSocketSession) {
        LOGGER.info("新添加webSocket连接 {} ", key);
        manager.put(key, webSocketSession);
    }

    public static void remove(String key) {
        LOGGER.info("移除webSocket连接 {} ", key);
        manager.remove(key);
    }

    public static WebSocketSession get(String key) {
        LOGGER.info("获取webSocket连接 {}", key);
        return manager.get(key);
    }

}
