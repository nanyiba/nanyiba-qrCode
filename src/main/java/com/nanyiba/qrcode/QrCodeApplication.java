package com.nanyiba.qrcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

/**
 * @Author 熊腾强
 * @Date 2021/7/30 3:26 下午
 * @Description
 */
@EnableCaching
@SpringBootApplication
public class QrCodeApplication {
    public static void main(String[] args) {
        SpringApplication.run(QrCodeApplication.class, args);
    }
}
